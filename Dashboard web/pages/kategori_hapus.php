<?php
include("koneksi.php");

if(isset($_GET)){
	$id = $_GET['id_kategori_ikan'];
	
	$sql = "DELETE FROM `kategori_ikan` WHERE `id_kategori_ikan` = $id";
	$eksekusi = mysqli_query($koneksi, $sql);
	if($eksekusi){
		?>
		<script>
			window.location = "kategori.php";
		</script>
		<?php
	}
	else{
		?>
		<script>
			alert('Data gagal dihapus');
			history.back(-1);
		</script>
		<?php
	}
}
else{
	?>
	<script>
		alert('Maaf, terjadi error.');
		history.back(-1);
	</script>
	<?php
}
?>