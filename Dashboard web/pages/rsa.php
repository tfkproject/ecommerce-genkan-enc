<?php
    //menggunakan library phpseclib
    set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
    
    include('Math/BigInteger.php');
    ///Start Algoritma RSA///
    
    //1. Pilih dua bilangan prima p ≠ q secara acak dan terpisah untuk tiap-tiap p dan q.
    $p = new Math_BigInteger("165403312992461714202656224102989012128913150423916492479515931001221851789131283507444724311012454832903616325403241855830790167398842268395528806239778485123385166708849285851907828550632968574120137622339023053337811061772911792183108714631577579182189028245414723904306400074654841827702823490530675173999");
    $q = new Math_BigInteger("179040704576183222112384467505114212885457365877158581280157533137639394367546283136013753139120909611172358155938856802200460147119301337666955366486642302953313718190725864675465571280738942074154322385171314795947704262737270871899848007555083045288679230467633534027255095669300363949519507096453381838439");
    
    //2. Hitung N = p q.
    $N = $p->multiply($q);
    
    //3. Hitung φ = (p-1)(q-1).
    $φ = $p->subtract(new Math_BigInteger("1"))->multiply($q->subtract(new Math_BigInteger("1")));
           
    //4. Pilih bilangan bulat koprima terkecil
    $e = new Math_BigInteger("3");
    while ($φ->gcd($e) > 1) {
        $e = $e->add(new Math_BigInteger("2"));
    }
    
    //5. Hitung d hingga d e ≡ 1 (mod φ)
    $d = $e->modInverse($φ);
    ///End Algoritma RSA///
    
    /*
    //Dekrip
    $dekrip = new Math_BigInteger("450960025016863888749126095598126419508245212270627953464");
    $str = $dekrip->powMod($d, $N);
    echo $str->toBytes();
    */
    
    /*Copyright (c) @T4ufik_Hidayat*/
?>
