<?php
session_start();
if (empty($_SESSION)) {
	header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else{
	include("koneksi.php");
?>
<!DOCTYPE html>
<html lang="en">

<?php
include("head.php");

$id_petani = $_GET['id_petani'];
?>

<body>

    <div id="wrapper">

	<?php include("nav.php"); ?>
	
	
	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Data Petani</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Edit Data
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="petani_edit_process.php" method="POST">
                                        <?php
        									$query = "select * from `petani` where `id_petani` = '$id_petani'";
		        							$eksekusi = mysqli_query($koneksi, $query);
				        					while($row = mysqli_fetch_array($eksekusi)){
						           		?>
						           		<input name="id_petani" class="form-control" type="hidden" value="<?php echo $row['id_petani']; ?>" >
                                        <div class="form-group">
                                            <label>Nama Petani</label>
                                            <input name="nama_petani" class="form-control" value="<?php echo $row['nama_petani']; ?>" placeholder="contoh: Joko">
                                        </div>
										<?php
				        				}
										?>
                                        <button name="submit" value="submit" type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php
	include("script.php");
	?>

</body>

</html>
<?php
}
?>