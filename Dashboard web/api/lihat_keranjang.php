<?php
// array for JSON response
$response = array();
// include db connect class
require 'connect.php';

$id_pelanggan = $_POST['id_pelanggan'];

if($result = $db->query("SELECT * FROM item_beli inner join keranjang on item_beli.id_keranjang = keranjang.id_keranjang inner join dagangan on item_beli.id_dagangan = dagangan.id_dagangan inner join pelanggan on item_beli.id_pelanggan = pelanggan.id_pelanggan inner join kategori_ikan on dagangan.id_kategori_ikan = kategori_ikan.id_kategori_ikan inner join petani on dagangan.id_petani = petani.id_petani where pelanggan.id_pelanggan = '$id_pelanggan'")){
	if($count = $result->num_rows){
		$response["daftar"] = array();
		
		while($row = $result->fetch_object()){
			$data = array();
			$data["id_item_beli"] = $row->id_item_beli;
			$data["id_keranjang"] = $row->id_keranjang;
			$data["id_dagangan"] = $row->id_dagangan;
			$data["id_petani"] = $row->id_petani;
			$data["nama_petani"] = $row->nama_petani;
			$data["id_kategori_ikan"] = $row->id_kategori_ikan;
			$data["nama_ikan"] = $row->nama_ikan;
			$data["jum_kg"] = $row->jum_kg;
			$data["harga_total"] = $row->harga_total;
			$data["foto"] = $row->foto_dagangan;
					
			
			array_push($response["daftar"], $data);
		}
		
		$response["success"] = 1;
		
		// echoing JSON response
		echo json_encode($response);
	}
		
		$result->free();
} else {
    // no datas found
    $response["success"] = 0;
    $response["message"] = "Tidak ada data ditemukan";
    // echo no users JSON
    echo json_encode($response);
}
?>