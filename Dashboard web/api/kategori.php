<?php
// array for JSON response
$response = array();
// include db connect class
require 'connect.php';

if($result = $db->query("SELECT * FROM kategori_ikan")){
	if($count = $result->num_rows){
		$response["daftar"] = array();
		
		while($row = $result->fetch_object()){
			$data = array();
			$data["id_kategori_ikan"] = $row->id_kategori_ikan;
			$data["nama_ikan"] = $row->nama_ikan;
			$data["foto"] = $row->foto;
					
			
			array_push($response["daftar"], $data);
		}
		
		$response["success"] = 1;
		
		// echoing JSON response
		echo json_encode($response);
	}
		
		$result->free();
} else {
    // no datas found
    $response["success"] = 0;
    $response["message"] = "Tidak ada data ditemukan";
    // echo no users JSON
    echo json_encode($response);
}
?>