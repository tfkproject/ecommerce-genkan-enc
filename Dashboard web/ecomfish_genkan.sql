-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 03, 2018 at 07:54 PM
-- Server version: 5.5.56-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecomfish_genkan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(8) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `email`, `password`) VALUES
(1, 'Administrator', 'admin@admin.com', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `dagangan`
--

CREATE TABLE `dagangan` (
  `id_dagangan` int(11) NOT NULL,
  `id_petani` int(11) NOT NULL,
  `id_kategori_ikan` int(11) NOT NULL,
  `harga_per_kg` int(11) NOT NULL,
  `foto_dagangan` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dagangan`
--

INSERT INTO `dagangan` (`id_dagangan`, `id_petani`, `id_kategori_ikan`, `harga_per_kg`, `foto_dagangan`) VALUES
(1, 1, 1, 40000, 'http://genkan.ecomfish.com/img/ikan_dagangan/Oreo_nilo_071011-0507_F_jtg.jpg'),
(2, 2, 1, 50000, 'http://genkan.ecomfish.com/img/ikan_dagangan/ikan_petani.jpg'),
(5, 3, 3, 40000, 'http://genkan.ecomfish.com/img/ikan_dagangan/logo.PNG'),
(6, 4, 5, 62000, 'http://genkan.ecomfish.com/img/ikan_dagangan/gambar-ikan-patin-ilustrasi.jpg'),
(7, 5, 5, 14500, 'http://genkan.ecomfish.com/img/ikan_dagangan/IkanPatin.jpg'),
(8, 6, 6, 15000, 'http://genkan.ecomfish.com/img/ikan_dagangan/ikanLele.jpg'),
(9, 7, 8, 30000, 'http://genkan.ecomfish.com/img/ikan_dagangan/ikanjelawat.png'),
(10, 8, 1, 25000, 'http://genkan.ecomfish.com/img/ikan_dagangan/IkanNila.jpg'),
(11, 9, 9, 25000, 'http://genkan.ecomfish.com/img/ikan_dagangan/klasifikasi-ikan-gabus.jpg'),
(12, 10, 10, 35000, 'http://genkan.ecomfish.com/img/ikan_dagangan/ikangurami.jpg'),
(13, 11, 11, 35000, 'http://genkan.ecomfish.com/img/ikan_dagangan/ikanbaung1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `item_beli`
--

CREATE TABLE `item_beli` (
  `id_item_beli` int(11) NOT NULL,
  `id_keranjang` int(11) NOT NULL,
  `id_dagangan` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `jum_kg` int(11) NOT NULL,
  `harga_total` int(11) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_beli`
--

INSERT INTO `item_beli` (`id_item_beli`, `id_keranjang`, `id_dagangan`, `id_pelanggan`, `jum_kg`, `harga_total`, `waktu`) VALUES
(28, 11, 7, 8, 1, 14500, '2017-12-21 07:08:19'),
(21, 8, 10, 4, 2, 50000, '2017-12-18 08:52:55'),
(19, 8, 7, 4, 1, 14500, '2017-12-18 07:25:19');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_ikan`
--

CREATE TABLE `kategori_ikan` (
  `id_kategori_ikan` int(11) NOT NULL,
  `nama_ikan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `foto` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kategori_ikan`
--

INSERT INTO `kategori_ikan` (`id_kategori_ikan`, `nama_ikan`, `foto`) VALUES
(1, 'Ikan Nila', 'http://genkan.ecomfish.com/img/ikan_kategori/nila.jpg'),
(5, 'Ikan Patin', 'http://genkan.ecomfish.com/img/ikan_kategori/Cara-Membedakan-Ikan-Patin-Jantan-dan-Betina-1.jpg'),
(6, 'Ikan Lele', 'http://genkan.ecomfish.com/img/ikan_kategori/ikanLele.jpg'),
(8, 'Ikan Jelawat', 'http://genkan.ecomfish.com/img/ikan_kategori/ikanjelawat.png'),
(9, 'Ikan Gabus', 'http://genkan.ecomfish.com/img/ikan_kategori/klasifikasi-ikan-gabus.jpg'),
(10, 'Ikan Gurami', 'http://genkan.ecomfish.com/img/ikan_kategori/ikangurami.jpg'),
(11, 'Ikan Baung', 'http://genkan.ecomfish.com/img/ikan_kategori/ikanbaung1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `id_keranjang` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `keranjang`
--

INSERT INTO `keranjang` (`id_keranjang`, `id_pelanggan`) VALUES
(8, 4),
(11, 8);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `nama_pelanggan` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `nomor_hp` text COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `nama_pelanggan`, `email`, `password`, `nomor_hp`, `alamat`) VALUES
(9, 'nanda', 'nanda@gmail.com', '158267354350243993992524262083318636839704', '3323446032557353023734493029338711779046265824592687720121584898837255645862759355464', '1645798526921291085283940925856129346529096'),
(7, 'puput', 'puput@gmail.com', '158267352617704702599781581687670262008875', '3323436567502617101670279757292991756517889767023461890060810766212805216389834954125', '25419413871291783816293832818425081'),
(8, 'aaa', 'aaa@aaa.com', '158267354350243993992524262083318636839704', '3323446032653633944144059580664707681159421269650772462078201037618347630401796288000', '450960025016863888749126095598126419508245212270627953464');

-- --------------------------------------------------------

--
-- Table structure for table `petani`
--

CREATE TABLE `petani` (
  `id_petani` int(11) NOT NULL,
  `nama_petani` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `petani`
--

INSERT INTO `petani` (`id_petani`, `nama_petani`) VALUES
(7, 'Zulasmi'),
(6, 'Budi'),
(5, 'Apri Izrozi'),
(8, 'Arham Muhardi'),
(9, 'Nofri Wahyudi'),
(10, 'Ilham Akbar'),
(11, 'Saidi');

-- --------------------------------------------------------

--
-- Table structure for table `record_item`
--

CREATE TABLE `record_item` (
  `id_record_item` int(11) NOT NULL,
  `id_item_beli` int(11) NOT NULL,
  `id_keranjang` int(11) NOT NULL,
  `id_dagangan` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `jum_kg` int(11) NOT NULL,
  `harga_total` int(11) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `record_item`
--

INSERT INTO `record_item` (`id_record_item`, `id_item_beli`, `id_keranjang`, `id_dagangan`, `id_pelanggan`, `jum_kg`, `harga_total`, `waktu`) VALUES
(1, 1, 1, 2, 8, 1, 50000, '2017-12-06 10:35:23'),
(2, 2, 1, 5, 8, 1, 40000, '2017-12-06 10:35:29'),
(3, 3, 2, 6, 9, 1, 62000, '2017-12-06 14:43:46'),
(4, 4, 2, 1, 9, 2, 80000, '2017-12-06 14:44:17'),
(5, 5, 3, 2, 10, 5, 250000, '2017-12-06 15:18:04'),
(6, 6, 4, 2, 11, 1, 50000, '2017-12-06 15:21:32'),
(7, 7, 4, 1, 11, 1, 40000, '2017-12-06 15:21:38'),
(8, 8, 4, 6, 11, 2, 124000, '2017-12-06 15:23:12'),
(9, 9, 4, 6, 11, 2, 124000, '2017-12-06 15:23:24'),
(10, 10, 4, 6, 11, 2, 124000, '2017-12-06 15:23:39'),
(11, 11, 4, 6, 11, 2, 124000, '2017-12-06 15:23:54'),
(12, 12, 5, 1, 11, 2, 80000, '2017-12-06 15:30:37'),
(14, 14, 5, 6, 11, 1, 62000, '2017-12-06 15:32:45'),
(15, 15, 6, 9, 4, 2, 60000, '2017-12-13 06:04:13'),
(16, 16, 7, 7, 4, 1, 14500, '2017-12-13 06:12:00'),
(17, 17, 7, 10, 4, 1, 25000, '2017-12-13 06:12:09'),
(18, 18, 7, 7, 4, 2, 29000, '2017-12-18 06:07:35'),
(19, 19, 8, 7, 4, 1, 14500, '2017-12-18 07:25:19'),
(21, 21, 8, 10, 4, 2, 50000, '2017-12-18 08:52:55'),
(24, 24, 9, 10, 5, 16, 400000, '2017-12-19 05:01:15'),
(25, 25, 10, 7, 5, 1, 14500, '2017-12-19 05:28:58'),
(26, 26, 10, 12, 5, 1, 35000, '2017-12-19 05:29:09'),
(27, 27, 10, 13, 5, 2, 70000, '2017-12-19 05:29:52'),
(28, 28, 11, 7, 8, 1, 14500, '2017-12-21 07:08:19'),
(29, 29, 12, 7, 9, 2, 29000, '2017-12-27 12:05:36'),
(30, 30, 13, 10, 7, 1, 25000, '2017-12-28 04:34:21'),
(31, 31, 13, 9, 7, 1, 30000, '2017-12-28 04:34:37'),
(32, 32, 14, 10, 9, 2, 50000, '2018-01-03 08:20:04'),
(33, 33, 15, 7, 9, 1, 14500, '2018-01-03 09:49:21'),
(34, 34, 15, 10, 9, 1, 25000, '2018-01-03 09:49:48'),
(35, 35, 16, 7, 9, 2, 29000, '2018-01-03 14:25:15'),
(36, 36, 16, 10, 9, 1, 25000, '2018-01-03 14:25:33'),
(37, 37, 17, 12, 9, 1, 35000, '2018-01-05 07:09:36'),
(38, 38, 17, 7, 9, 1, 14500, '2018-01-17 07:34:51'),
(39, 39, 18, 7, 9, 2, 29000, '2018-01-18 15:48:44'),
(40, 40, 19, 9, 9, 2, 60000, '2018-01-27 06:50:51'),
(41, 41, 19, 10, 9, 1, 25000, '2018-01-27 06:51:10');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_keranjang` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `file_bukti` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_keranjang`, `id_pelanggan`, `total_bayar`, `waktu`, `status`, `file_bukti`) VALUES
(6, 12, 9, 29000, '2017-12-27 12:05:52', 'Y', 'http://genkan.ecomfish.com/img/pelanggan/IMG_1514904712.jpg'),
(7, 13, 7, 55000, '2017-12-28 04:34:50', 'Y', 'http://genkan.ecomfish.com/img/pelanggan/IMG_1514435735.jpg'),
(8, 14, 9, 50000, '2018-01-03 08:20:12', 'Y', 'http://genkan.ecomfish.com/img/pelanggan/IMG_1514967907.jpg'),
(9, 15, 9, 39500, '2018-01-03 09:52:20', 'W', 'http://genkan.ecomfish.com/img/pelanggan/IMG_1514987237.jpg'),
(10, 16, 9, 54000, '2018-01-03 14:26:04', 'N', 'Belum upload'),
(11, 17, 9, 49500, '2018-01-17 07:34:57', 'N', 'Belum upload'),
(12, 18, 9, 29000, '2018-01-18 15:48:57', 'N', 'Belum upload'),
(13, 19, 9, 85000, '2018-01-27 06:51:45', 'N', 'Belum upload');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `dagangan`
--
ALTER TABLE `dagangan`
  ADD PRIMARY KEY (`id_dagangan`);

--
-- Indexes for table `item_beli`
--
ALTER TABLE `item_beli`
  ADD PRIMARY KEY (`id_item_beli`);

--
-- Indexes for table `kategori_ikan`
--
ALTER TABLE `kategori_ikan`
  ADD PRIMARY KEY (`id_kategori_ikan`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `petani`
--
ALTER TABLE `petani`
  ADD PRIMARY KEY (`id_petani`);

--
-- Indexes for table `record_item`
--
ALTER TABLE `record_item`
  ADD PRIMARY KEY (`id_record_item`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dagangan`
--
ALTER TABLE `dagangan`
  MODIFY `id_dagangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `item_beli`
--
ALTER TABLE `item_beli`
  MODIFY `id_item_beli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `kategori_ikan`
--
ALTER TABLE `kategori_ikan`
  MODIFY `id_kategori_ikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id_keranjang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `petani`
--
ALTER TABLE `petani`
  MODIFY `id_petani` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `record_item`
--
ALTER TABLE `record_item`
  MODIFY `id_record_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
