package app.tfkproject.ecomfish.app;

import android.os.StrictMode;

public class Config {
    //link server
    public static final String URL = "http://genkan.ecomfish.com/api/";

    public void izinNetworkPolicy(){
        //dapatkan  izin untuk melakukan thread policy (proses Background AsycnTask)
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
}
