package app.tfkproject.ecomfish.app;

import android.util.Log;

import java.math.BigInteger;

public class RSA {

    private BigInteger p, q, N, φ, d, e;

    private int bitlen = 1024;

    public RSA(BigInteger new_N, BigInteger new_e) {
        N = new_N;
        e = new_e;
    }

    public RSA(int bits) {
        bitlen = bits;
        //1. Pilih dua bilangan prima p ≠ q secara acak dan terpisah untuk tiap-tiap p dan q.
        BigInteger p = new BigInteger("165403312992461714202656224102989012128913150423916492479515931001221851789131283507444724311012454832903616325403241855830790167398842268395528806239778485123385166708849285851907828550632968574120137622339023053337811061772911792183108714631577579182189028245414723904306400074654841827702823490530675173999");
        BigInteger q = new BigInteger("179040704576183222112384467505114212885457365877158581280157533137639394367546283136013753139120909611172358155938856802200460147119301337666955366486642302953313718190725864675465571280738942074154322385171314795947704262737270871899848007555083045288679230467633534027255095669300363949519507096453381838439");
        //2. Hitung N = p q.
        N = p.multiply(q);
        //3. Hitung φ = (p-1)(q-1).
        φ = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
        //4. Pilih bilangan bulat koprima terkecil
        e = new BigInteger("3");
        while (φ.gcd(e).intValue() > 1) {
            e = e.add(new BigInteger("2"));
        }
        //5. Hitung d hingga d e ≡ 1 (mod φ).
        d = e.modInverse(φ);

        Log.e("N", "=: "+N);
        Log.e("φ", "=: "+φ);
        Log.e("e", "=: "+e);
        Log.e("d", "=: "+d);
    }

    /** Encrypt the given plaintext message. */
    public synchronized String encrypt(String message) {
        return (new BigInteger(message.getBytes())).modPow(e, N).toString();
    }

    /** Encrypt the given plaintext message. */
    public synchronized BigInteger encrypt(BigInteger message) {
        return message.modPow(e, N);
    }

    /** Decrypt the given ciphertext message. */
    public synchronized String decrypt(String message) {
        return new String((new BigInteger(message)).modPow(d, N).toByteArray());
    }

    /** Decrypt the given ciphertext message. */
    public synchronized BigInteger decrypt(BigInteger message) {
        return message.modPow(d, N);
    }

    /** Return the modulus. */
    public synchronized BigInteger getN() {
        return N;
    }

    /** Return the public key. */
    public synchronized BigInteger getE() {
        return e;
    }
}
