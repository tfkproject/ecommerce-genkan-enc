package app.tfkproject.ecomfish.model;

public class IkanKategori {
    private String id_kategori_ikan;
    private String nama_ikan;
    private String link_foto;

    public String getId_kategori_ikan() {
        return id_kategori_ikan;
    }

    public void setId_kategori_ikan(String id_kategori_ikan) {
        this.id_kategori_ikan = id_kategori_ikan;
    }

    public String getNama_ikan() {
        return nama_ikan;
    }

    public void setNama_ikan(String nama_ikan) {
        this.nama_ikan = nama_ikan;
    }

    public String getLink_foto() {
        return link_foto;
    }

    public void setLink_foto(String link_foto) {
        this.link_foto = link_foto;
    }
}
